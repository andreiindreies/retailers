<?php
namespace Training\Retailers\Observer;

use Magento\Catalog\Model\Product;
use Magento\Customer\Model\Customer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use Magento\Sales\Model\Order;
use Magento\Sales\Api\Data\OrderItemInterface;
use Magento\Framework\Exception\MailException;

use Training\Retailers\Helper\Email as EmailHelper;
use Training\Retailers\Model\Retailers;

/**
 * Class SendRetailerEmail
 *
 * @package Training\Retailers\Observer
 */
class SendRetailerEmail implements ObserverInterface
{
    /** @var Product */
    protected $product;

    /** @var Retailers */
    protected $retailers;

    /** @var EmailHelper  */
    protected $email;

    /** @var Order  */
    protected $order;

    /**
     * SendRetailerEmail constructor.
     *
     * @param Product $product
     * @param Retailers $retailers
     * @param EmailHelper $email
     * @param Order $order
     */
    public function __construct(Product $product, Retailers $retailers, EmailHelper $email, Order $order)
    {
        $this->product = $product;
        $this->retailers = $retailers;
        $this->email = $email;
        $this->order = $order;
    }

    /**
     * @param Observer $observer
     * @throws MailException
     */
    public function execute(Observer $observer)
    {
        $data= array();

        /** @var Order $order */
        $orderId = $observer->getEvent()->getOrderIds();
        $order = $this->order->load($orderId);

        /** @var Customer */
        $customerName =  $order->getCustomerFirstname();

        /** @var  $items */
        $items = $order->getItems();

        /** @var OrderItemInterface $item */
        foreach ($items as $item) {
            if ($retailerAttr = $this->product->load($item->getProductId())->getCustomAttribute('retailer')) {
               $id = $retailerAttr->getValue();
            } else {
                continue;
            }

            $data[$id][] = array( 'name' => $item->getName(), 'qty' => $item->getQtyOrdered());
        }

        foreach ($data as $key => $items) {
            /** @var Retailers */
            $retailer = $this->retailers->load($key);

            $receiverInfo = array('name' => $retailer->getName(), 'email' => $retailer->getEmail());
            $emailTempVariables['retailerName'] = $retailer->getName();
            $emailTempVariables['customerName'] = $customerName;
            $emailTempVariables['items'] = $items;

            $this->email->sendCustomMail(
                $emailTempVariables,
                $receiverInfo
            );
        }
    }
}