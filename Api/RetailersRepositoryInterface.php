<?php

namespace Training\Retailers\Api;

use Training\Retailers\Api\Data\RetailersInterface;

interface RetailersRepositoryInterface
{
    /**
     * Create or update retailer's data.
     *
     * @param RetailersInterface $retailer
     * @return mixed
     */
    public function save(RetailersInterface $retailer);

    /**
     * Retrieve retailer's data.
     *
     * @param $retailer
     * @return mixed
     */
    public function getById($retailer);

    /**
     * Delete retailer's data by retailer.
     *
     * @param RetailersInterface $retailer
     * @return mixed
     */
    public function delete(RetailersInterface $retailer);

    /**
     * Delete retailer by ID.
     *
     * @param $retailer
     * @return mixed
     */
    public function deleteById($retailer);
}