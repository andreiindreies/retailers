<?php

namespace Training\Retailers\Api;

/**
 * Interface WebServicesInterface
 *
 * @package Training\Retailers\Api
 */
interface WebServicesInterface
{
    /**
     * Retrieve a retailer by name,postcode and country.
     *
     * @param string $name
     * @param int $postCode
     * @param string $country
     *
     * @return array
     */
    public function getRetailersByParams($name = null, $postCode = null, $country = null);

    /**
     * Add a new retailer by params given.
     *
     * @param string $name
     * @param string $email
     * @param string $country
     * @param int $postcode
     * @param float $longitude
     * @param float $latitude
     *
     * @return string
     */
    public function setNewRetailer($name, $email, $country, $postcode, $longitude, $latitude);
}