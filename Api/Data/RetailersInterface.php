<?php

namespace Training\Retailers\Api\Data;

/**
 * Interface RetailersInterface
 * @package Training\Retailers\Api\Data
 */
interface RetailersInterface {

    const ENTITY_ID = 'entity_id';
    const NAME = 'name';
    const EMAIL = 'email';
    const COUNTRY = 'country';
    const POSTCODE = 'postcode';
    const UPDATE_TIME = 'update_at';
    const CREATED_AT = 'created_at';

    /**
     * Get EntityId.
     *
     * @return int
     */
    public function getEntityId();

    /**
     * Set EntityId.
     */
    public function setEntityId($entityId);

    /**
     * Get Title.
     *
     * @return varchar
     */
    public function getName();

    /**
     * Set Title.
     */
    public function setName($name);

    /**
     * Get Content.
     *
     * @return varchar
     */
    public function getEmail();

    /**
     * Set Content.
     */
    public function setEmail($email);

    /**
     * Get Country.
     *
     * @return varchar
     */
    public function getCountry();

    /**
     * Set Country.
     */
    public function setCountry($country);


    /**
     * Get Publish Date.
     *
     * @return varchar
     */
    public function getPostCode();

    /**
     * Set PublishDate.
     */
    public function setPostCode($postCode);

    /**
     * Get UpdateTime.
     *
     * @return varchar
     */
    public function getUpdateAt();

    /**
     * Set UpdateTime.
     */
    public function setUpdateAt($updateAt);

    /**
     * Get CreatedAt.
     *
     * @return varchar
     */
    public function getCreatedAt();

    /**
     * Set CreatedAt.
     */
    public function setCreatedAt($createdAt);
}