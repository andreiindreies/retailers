<?php

namespace Training\Retailers\Model\ResourceModel\Retailers;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 * @package Training\Retailers\Model\ResourceModel\Retailers
 */
class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'entity_id';

    /**
     * Define resource model.
     */
    protected function _construct()
    {
        $this->_init('Training\Retailers\Model\Retailers', 'Training\Retailers\Model\ResourceModel\Retailers');
    }
}