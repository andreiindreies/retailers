<?php

namespace Training\Retailers\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;
use Magento\Framework\Stdlib\DateTime\DateTime;

/**
 * Class Retailers
 * @package Training\Retailers\Model\ResourceModel
 */
class Retailers extends AbstractDb
{

    protected $idFieldName = 'entity_id';
    protected $date;

    /**
     * Retailers constructor.
     * @param Context $context
     * @param DateTime $date
     * @param null $resourcePrefix
     */
    public function __construct(
        Context $context,
        DateTime $date,
        $resourcePrefix = null
    )
    {
        parent::__construct($context, $resourcePrefix);
        $this->date = $date;
    }

    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init('mm_retailers', $this->idFieldName);
    }
}