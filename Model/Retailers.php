<?php

namespace Training\Retailers\Model;

use Magento\Framework\Model\AbstractModel;
use Training\Retailers\Api\Data\RetailersInterface;

/**
 * Class Retailers
 * @package Training\Retailers\Model
 */
class Retailers extends AbstractModel implements RetailersInterface
{
    /**
     * CMS page cache tag.
     */
    const CACHE_TAG = 'mm_retailers';

    /**
     * @var string
     */
    protected $_cacheTag = 'mm_retailers';

    /**
     * Prefix of model events names.
     *
     * @var string
     */
    protected $_eventPrefix = 'mm_retailers';

    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init('Training\Retailers\Model\ResourceModel\Retailers');
    }
    /**
     * Get EntityId.
     *
     * @return int
     */
    public function getEntityId()
    {
        return $this->getData(self::ENTITY_ID);
    }

    /**
     * Set EntityId.
     */
    public function setEntityId($entityId)
    {
        return $this->setData(self::ENTITY_ID, $entityId);
    }

    /**
     * Get Name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->getData(self::NAME);
    }

    /**
     * Set Name.
     */
    public function setName($name)
    {
        return $this->setData(self::NAME, $name);
    }

    /**
     * Get Email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->getData(self::EMAIL);
    }

    /**
     * Set Email.
     */
    public function setEmail($email)
    {
        return $this->setData(self::EMAIL, $email);
    }

    /**
     * Get Country.
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->getData(self::COUNTRY);
    }

    /**
     * Set Country.
     */
    public function setCountry($country)
    {
        return $this->setData(self::COUNTRY, $country);
    }

    /**
     * Get post code.
     *
     * @return string
     */

    public function getPostCode()
    {
        return $this->getData(self::POSTCODE);
    }

    /**
     * Set post code.
     */
    public function setPostCode($postCode)
    {
        return $this->setData(self::POSTCODE, $postCode);
    }

    /**
     * Get UpdateTime.
     *
     * @return string
     */
    public function getUpdateAt()
    {
        return $this->getData(self::UPDATE_TIME);
    }

    /**
     * Set UpdateTime.
     */
    public function setUpdateAt($updateAt)
    {
        return $this->setData(self::UPDATE_TIME, $updateAt);
    }

    /**
     * Get CreatedAt.
     *
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * Set CreatedAt.
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }
}