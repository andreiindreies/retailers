<?php

namespace Training\Retailers\Model;

use Training\Retailers\Helper\TokenGenerator;
use Training\Retailers\Model\ResourceModel\Retailers\Collection;
use Training\Retailers\Model\ResourceModel\Retailers\CollectionFactory;
use Training\Retailers\Model\RetailersFactory;
use Training\Retailers\Model\Retailers;
use Training\Retailers\Api\RetailersRepositoryInterface;
use Training\Retailers\Api\WebServicesInterface;

use Zend\Http\PhpEnvironment\Request;

/**
 * Class WebServices
 * @package Training\Retailers\Model
 */
class WebServices implements WebServicesInterface
{
    /** @var Collection  */
    protected $retailersCollection;

    /** @var RetailersRepositoryInterface  */
    protected $retailersRepo;

    /** @var Retailers  */
    protected $retailersModelFactory;

    /** @var RequestInterface */
    protected $request;

    /** @var TokenGenerator */
    protected $tokenGenerator;

    /**
     * WebServices constructor.
     *
     * @param RetailersRepositoryInterface $retailersRepository
     * @param CollectionFactory $collection
     * @param RetailersFactory $retailer
     */
    public function __construct(RetailersRepositoryInterface $retailersRepository,
                                CollectionFactory $collection,
                                RetailersFactory $retailer,
                                Request $request,
                                TokenGenerator $token)
    {
        $this->tokenGenerator = $token;
        $this->retailersRepo = $retailersRepository;
        $this->retailersCollection = $collection;
        $this->retailersModelFactory = $retailer;
        $this->request = $request;

        // Restrict access if the token is not authorized.
        if (!$this->isAllowed()) {
            die('You are not authorized to use this api.');
        };
    }

    /**
     * Retrieve a retailer
     *
     * @param null $name
     * @param null $postCode
     * @param null $country
     *
     * @return array
     */
    public function getRetailersByParams($name = null, $postCode = null, $country = null)
    {
        $result = array();
        $params = array();
        $retailersEntity = $this->retailersCollection->create();

        if ($name) {
            $params['name'] = $name;
        }

        if ($postCode) {
            $params['postcode'] = $postCode;
        }

        if ($country) {
            $params['country'] = $country;
        }

        if ($params) {
            foreach ($params as $key => $value) {
                $retailersEntity->addFieldToFilter($key, $value);
            }
        }

        foreach ($retailersEntity->getItems() as $value) {
           $result[] = $value->getData();
        }

        return $result;
    }

    /**
     * Save the a new retail in database.
     *
     * @param $name
     * @param $email
     * @param $country
     * @param $postcode
     * @param $longitude
     * @param $latitude
     * @return bool|mixed|string
     */
    public function setNewRetailer($name, $email, $country, $postcode, $longitude, $latitude)
    {
        $params = array(
            'name' => $name,
            'email' => $email,
            'country' => $country,
            'postcode' => $postcode,
            'longitude' => $longitude,
            'latitude' => $latitude
        );

        try {
            if (!$this->areParamsValidForRetailer($params)) {
                throw new \Exception('Params are not valid.');
            }

            /** @var Retailers $newRetailer */
            $newRetailer = $this->retailersModelFactory->create();
            $newRetailer->setData($params);
            $newRetailer->save();
        } catch(\Exception $e) {
            return $e->getMessage();
        }

        return 'Success';
    }

    /**
     * Verify if params are valid for a new retailer.
     *
     * @param array $params
     * @return bool
     */
    protected function areParamsValidForRetailer(array $params)
    {
        foreach ($params as $key => $value) {
            if ($this->isStringParam($key)) {
                if (!is_string($value)) {
                    return false;
                }
            }

            if ($this->isNumericParam($key)) {
                if (!is_numeric($value)) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Verify if a param should be string.
     *
     * @param $param
     * @return bool
     */
    private function isStringParam($param)
    {
        return in_array($param, array('name', 'email', 'country'));
    }

    /**
     * Verify if a param should be numeric.
     *
     * @param $param
     * @return bool
     */
    private function isNumericParam($param)
    {
        return in_array($param, array('postcode', 'longitude', 'latitude'));
    }

    /**
     * Verify if the token from request is allowed to use api methods.
     *
     * @return bool
     */
    protected function isAllowed()
    {
        $token = $this->request->getHeader('Authorization')->getFieldValue();
        return $this->tokenGenerator->isTokenValid($token);
    }
}