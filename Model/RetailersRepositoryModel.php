<?php

namespace Training\Retailers\Model;

use Training\Retailers\Api\RetailersRepositoryInterface;
use Training\Retailers\Api\Data\RetailersInterface;

class RetailersRepositoryModel implements RetailersRepositoryInterface
{
    /** @var RetailersInterface * */
    protected $retailerModel;

    /**
     * RetailersRepositoryModel constructor.
     * @param RetailersInterface $retailer
     */
    public function __construct(RetailersInterface $retailer)
    {
        $this->retailerModel = $retailer;
    }

    /**
     * Create or update retailer's data.
     *
     * @param RetailersInterface $retailer
     * @return mixed
     */
    public function save(RetailersInterface $retailer)
    {
        return $retailer->save();
    }

    /**
     * Retrieve retailer's data.
     *
     * @param $retailer
     * @return mixed
     */
    public function getById($retailerId)
    {
        return $this->retailerModel->load($retailerId);
    }

    /**
     * Delete retailer's data by retailer.
     *
     * @param RetailersInterface $retailer
     * @return mixed
     */
    public function delete(RetailersInterface $retailer)
    {
        return $this->deleteById($retailer->getEntityId());
    }

    /**
     * Delete retailer by ID.
     *
     * @param $retailer
     * @return mixed
     */
    public function deleteById($retailerId)
    {
        $this->retailerModel->load($retailerId);
        return $this->retailerModel->delete();
    }
}