<?php

namespace Training\Retailers\Model\Lists;

use Training\Retailers\Model\ResourceModel\Retailers\CollectionFactory;

use Magento\Ui\DataProvider\AbstractDataProvider;

/**
 * Class DataProvider
 * @package Training\Retailers\Model\Lists
 */
class DataProvider extends AbstractDataProvider
{
    /**
     * DataProvider constructor.
     * @param $name
     * @param $primaryFieldName
     * @param $requestFieldName
     * @param CollectionFactory $retailersCollectionFactory
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        string $name,
        string $primaryFieldName,
        string $requestFieldName,
        CollectionFactory $retailersCollectionFactory,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $retailersCollectionFactory->create();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * Get retailer data
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }

        $items = $this->collection->getItems();
        $this->loadedData = array();

        foreach ($items as $retailer) {
            $this->loadedData[$retailer->getId()]['retailer'] = $retailer->getData();
        }

        return $this->loadedData;
    }
}
