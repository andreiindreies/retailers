<?php

namespace Training\Retailers\Model\Entity\Attribute\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

use Training\Retailers\Model\ResourceModel\Retailers\CollectionFactory;
use Training\Retailers\Model\ResourceModel\Retailers\Collection;

/**
 * Class RetailersOptionList
 * @package Training\Retailers\Model\Entity\Attribute\Source
 */
class RetailersOptionList extends AbstractSource
{
    /** @var array  */
    protected $retailersList = array();
    /** @var Collection */
    protected $retailersCollection;

    /**
     * RetailersOptionList constructor.
     *
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(CollectionFactory $collectionFactory)
    {
        $this->retailersCollection = $collectionFactory->create();
    }

    /**
     * Retrieve retailer's option list.
     *
     * @return array
     */
    public function getAllOptions()
    {
        // build the retailer's list if the array is empty.
        if (!$this->retailersList) {
            $this->createRetailersList();
        }

        return $this->retailersList;
    }

    /**
     * Build retailer's list from retailer's collection.
     */
    private function createRetailersList()
    {
        $this->retailersList[] = ['label' => __('None'), 'value' => null];
        $allRetailersData = $this->retailersCollection->getItems();

        foreach ($allRetailersData as $key => $item) {
            $this->retailersList[] =
                ['label' => __($item->getName()), 'value' => $item->getEntityId()];
        }
    }
}