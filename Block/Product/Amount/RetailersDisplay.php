<?php
namespace Training\Retailers\Block\Product\Amount;

use Magento\Catalog\Model\Product;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\View\Element\Template;

use Training\Retailers\Api\RetailersRepositoryInterface;

class RetailersDisplay extends Template
{
    /** @var Registry */
    protected $registry;

    /** @var RetailersRepositoryInterface  */
    protected $retailersRepo;

    /** @var Product */
    private $product;

    /**
     * RetailersDisplay constructor.
     * @param Template\Context $context
     * @param Registry $registry
     * @param RetailersRepositoryInterface $retailersRepository
     * @param array $data
     */
    public function __construct(Context $context,
                                Registry $registry,
                                RetailersRepositoryInterface $retailersRepository,
                                array $data)
    {
        $this->registry = $registry;
        $this->retailersRepo = $retailersRepository;

        parent::__construct($context, $data);
    }

    /**
     * Retrieve Retailer's object
     * @return mixed|null
     */
    public function getRetailer()
    {
        $attribute = $this->getProduct()->getCustomAttribute('retailer');
        $retailer = $attribute ? $this->retailersRepo->getById($attribute->getValue()) : null;

        return $retailer;
    }

    /**
     * Retrieve product in use.
     *
     * @return Product|mixed
     * @throws LocalizedException
     */
    private function getProduct()
    {
        if (is_null($this->product)) {
            $this->product = $this->registry->registry('product');

            if (!$this->product->getId()) {
                throw new LocalizedException(__('Failed to initialize product'));
            }
        }

        return $this->product;
    }
}