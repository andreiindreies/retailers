<?php

namespace Training\Retailers\Controller\Adminhtml\Lists;

use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;

use Training\Retailers\Api\RetailersRepositoryInterface;

/**
 * Class Save
 * @package Training\Retailers\Controller\Adminhtml\Lists
 */
class Save extends Action
{
    /** @var RetailersRepositoryInterface */
    protected $retailerRepo;

    /**
     * Save constructor.
     * @param Action\Context $context
     */
    public function __construct(Action\Context $context, RetailersRepositoryInterface $retailersRepository)
    {
        $this->retailerRepo = $retailersRepository;
        parent::__construct($context);
    }

    /**
     * @return ResponseInterface|Redirect|ResultInterface
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $requestedRetailer = $this->getRequest()->getParam('retailer');

        if ($retailer = $this->retailerRepo->getById($id)) {
            $retailer->setData($requestedRetailer);
            $this->retailerRepo->save($retailer);

            $resultRedirect = $this->resultRedirectFactory->create();

            return $resultRedirect->setPath('*/*/index', array('_current' => true));
        }
    }
}