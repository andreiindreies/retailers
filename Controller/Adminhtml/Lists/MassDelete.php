<?php

namespace Training\Retailers\Controller\Adminhtml\Lists;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\ResponseInterface;

use Training\Retailers\Model\ResourceModel\Retailers\CollectionFactory;
use Training\Retailers\Api\RetailersRepositoryInterface;

class MassDelete extends Action
{
    /** @var Filter **/
    protected $filter;
    /** @var CollectionFactory  */
    protected $collectionFactory;
    /** @var RetailersRepositoryInterface */
    protected $retailersRepo;

    /**
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(Context $context,
                                Filter $filter,
                                CollectionFactory $collectionFactory,
                                RetailersRepositoryInterface $retailersRepository

    ) {
        $this->retailersRepo = $retailersRepository;
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        parent::__construct($context);
    }

    /**
     * Dispatch request
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $collection = $this->filter->getCollection($this->collectionFactory->create());
        $collectionSize = $collection->getSize();

        foreach ($collection as $retailer) {
            $id = $retailer['entity_id'];
            $item = $this->retailersRepo->getById($id);
            $item->delete();
        }

        $this->messageManager->addSuccessMessage(__('A total of %1 element(s) have been deleted.', $collectionSize));
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        return $resultRedirect->setPath('*/*/index');
    }
}
