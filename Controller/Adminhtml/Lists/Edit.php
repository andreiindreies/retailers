<?php

namespace Training\Retailers\Controller\Adminhtml\Lists;

use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;

use Training\Retailers\Model\RetailersFactory;

class Edit extends Action
{
    /** @var Registry  */
    private $coreRegistry;
    /** @var RetailersFactory  */
    private $retailersFactory;

    public function __construct(
        Context $context,
        Registry $coreRegistry,
        RetailersFactory $retailersFactory
    ) {
        parent::__construct($context);
        $this->coreRegistry = $coreRegistry;
        $this->retailersFactory = $retailersFactory;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     */
    public function execute()
    {
        $rowId = (int) $this->getRequest()->getParam('id');
        $rowData = $this->retailersFactory->create();

        if ($rowId) {
            $rowData = $rowData->load($rowId);
            $rowTitle = $rowData->getTitle();
            if (!$rowData->getEntityId()) {
                $this->messageManager->addErrorMessage(__('row data no longer exist.'));
                $this->_redirect('retailers/lists/rowdata');
                return;
            }
        }

        $this->coreRegistry->register('row_data', $rowData);
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $title = $rowId ? __('Edit Row Data ').$rowTitle : __('Add Row Data');
        $resultPage->getConfig()->getTitle()->prepend($title);

        return $resultPage;
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Training_Retailers::edit');
    }
}