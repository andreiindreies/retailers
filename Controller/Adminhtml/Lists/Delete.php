<?php

namespace Training\Retailers\Controller\Adminhtml\Lists;

use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use Magento\Backend\App\Action;

use Training\Retailers\Model\Retailers;
use Training\Retailers\Api\RetailersRepositoryInterface;

class Delete extends Action
{
    /** @var RetailersRepositoryInterface */
    protected $retailerRepo;

    public function __construct(Action\Context $context, RetailersRepositoryInterface $retailersRepository)
    {
        $this->retailerRepo = $retailersRepository;
        parent::__construct($context);
    }
    /**
     * @return ResponseInterface|Redirect|ResultInterface
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $retailer = $this->retailerRepo->getById($id);

        if (!($retailer)) {
            $this->messageManager->addErrorMessage(__('Unable to proceed. Please, try again.'));
            $resultRedirect = $this->resultRedirectFactory->create();

            return $resultRedirect->setPath('*/*/index', array('_current' => true));
        }

        try{
            $retailer->delete();
            $this->messageManager->addSuccessMessage(__('Your retailer has been deleted !'));
        } catch (Exception $e) {
            $this->messageManager->addErrorMessage(__('Error while trying to delete retailer: '));
            $resultRedirect = $this->resultRedirectFactory->create();

            return $resultRedirect->setPath('*/*/index', array('_current' => true));
        }

        $resultRedirect = $this->resultRedirectFactory->create();

        return $resultRedirect->setPath('*/*/index', array('_current' => true));
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Training_Retailers::delete');
    }
}