<?php

namespace Training\Retailers\Setup;

use Magento\Catalog\Model\Product;
use Magento\Eav\Setup\EavSetup;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Model\Config;

/**
 * This adds the new custom attribute for customers entity.
 * Class InstallData
 * @package Training\Retailers\Setup
 */
class InstallData implements InstallDataInterface
{
    const CUSTOM_ATTRIBUTE_CODE = 'retailer';

    /** @var EavSetup  */
    private $eavSetup;
    /** @var Config  */
    private $eavConfig;

    /**
     * InstallData constructor.
     * @param EavSetup $eavSetup
     * @param Config $config
     */
    public function __construct(EavSetup $eavSetup, Config $config)
    {
        $this->eavSetup = $eavSetup;
        $this->eavConfig = $config;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function Install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $this->eavSetup->addAttribute(Product::ENTITY, self::CUSTOM_ATTRIBUTE_CODE,
            [
                'type' => 'int',
                'label' => 'Retailer',
                'input' => 'select',
                'visible' => true,
                'source' => 'Training\Retailers\Model\Entity\Attribute\Source\RetailersOptionList',
                'required' => false,
                'position' => 150,
                'is_used_in_grid' => true,
                'is_visible_in_grid' => true,
                'is_filterable_in_grid' => true,
                'sort_order' => 150,
                'system' => false,
                'visible_on_front' => true,
                'unique' => false,
                'option' => [
                    'values' => [],
                ]
            ]
        );

        $retailer = $this->eavConfig->getAttribute(Product::ENTITY, self::CUSTOM_ATTRIBUTE_CODE);
        $retailer->setData('used_in_forms', ['adminhtml_catalog']);
        $retailer->save();

        $setup->endSetup();
    }

}
