<?php
namespace Training\Retailers\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Area;
use Magento\Store\Api\Data\StoreInterface;

/**
 * Class Email
 *
 * @package Training\Retailers\Helper
 */
class Email extends AbstractHelper
{
    /** xml path from config xml */
    const XML_PATH_EMAIL_TEMPLATE_FIELD  = 'retailers/retailers_group/retailer_email_template';

    /** @var Context  */
    protected $scopeConfig;

    /** @var StoreManagerInterface  */
    protected $storeManager;

    /** @var StateInterface  */
    protected $inlineTranslation;

    /** @var TransportBuilder  */
    protected $transportBuilder;

    /** @var string */
    protected $tempId;

    /**
     * Email constructor.
     *
     * @param Context $context
     * @param StoreManagerInterface $storeManager
     * @param StateInterface $inlineTranslation
     * @param TransportBuilder $transportBuilder
     */
    public function __construct(
        Context $context,
        StoreManagerInterface $storeManager,
        StateInterface $inlineTranslation,
        TransportBuilder $transportBuilder
    ) {
        $this->scopeConfig = $context;
        parent::__construct($context);
        $this->storeManager = $storeManager;
        $this->inlineTranslation = $inlineTranslation;
        $this->transportBuilder = $transportBuilder;
    }

    /**
     * Return store configuration value of your template field that which id you set for template
     *
     * @param string $path
     * @param int $storeId
     * @return mixed
     */
    protected function getConfigValue($path, $storeId)
    {
        return $this->scopeConfig->getValue(
            $path,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Return template id according to store
     *
     * @param $xmlPath
     * @return mixed
     * @throws NoSuchEntityException
     */
    public function getTemplateId($xmlPath)
    {
        return $this->getConfigValue($xmlPath, $this->getStore()->getStoreId());
    }

    /**
     * @param $emailTemplateVariables
     * @param $receiverInfo
     * @return $this
     * @throws NoSuchEntityException
     */
    public function generateTemplate($emailTemplateVariables,$receiverInfo)
    {
        $template =  $this->transportBuilder->setTemplateIdentifier($this->tempId)
            ->setTemplateOptions(
                [
                    'area' => Area::AREA_FRONTEND,
                    'store' => $this->storeManager->getStore()->getId(),
                ]
            )
            ->setTemplateVars($emailTemplateVariables)
            ->setFrom(array('email' => $this->getStoreEmail(), 'name' => $this->getStoreName()))
            ->addTo($receiverInfo['email'],$receiverInfo['name']);

        return $this;
    }

    /**
     * Send custom mail method.
     *
     * @param $emailTemplateVariables
     * @param $receiverInfo
     * @throws \Magento\Framework\Exception\MailException
     */
    public function sendCustomMail($emailTemplateVariables,$receiverInfo)
    {
        $this->tempId = $this->getTemplateId(self::XML_PATH_EMAIL_TEMPLATE_FIELD);
        $this->inlineTranslation->suspend();
        $this->generateTemplate($emailTemplateVariables, $receiverInfo);
        $transport = $this->transportBuilder->getTransport();
        $transport->sendMessage();
        $this->inlineTranslation->resume();
    }

    /**
     * Retrieve Store.
     *
     * @return StoreInterface
     * @throws NoSuchEntityException
     */
    protected function getStore()
    {
        return $this->storeManager->getStore();
    }

    /**
     * Retrieve store name.
     * @return string
     */
    protected function getStoreName()
    {
        return $this->scopeConfig->getValue(
            'trans_email/ident_sales/name',
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Retrieve store email.
     *
     * @return string
     */
    protected function getStoreEmail()
    {
        return $this->scopeConfig->getValue(
            'trans_email/ident_sales/email',
            ScopeInterface::SCOPE_STORE
        );
    }
}