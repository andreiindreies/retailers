<?php

namespace Training\Retailers\Helper;

use Magento\Framework\Filesystem\Driver\File;

/**
 * Class TokenGenerator
 *
 * @package Training\Retailers\Helper
 */
class TokenGenerator
{
    // Token file path and name.
    const TOKEN_FILE_NAME = 'app/code/Training/Retailers/Helper/token.txt';

    // How much time token will be available in days.
    const TOKEN_EXPIRE_TIME = '1 day';

    /** @var string  */
    protected $now;

    /** @var string  */
    protected $expireDate;

    /** @var File  */
    protected $fileDriver;

    /** @var array */
    protected static $token;

    /**
     * TokenGenerator constructor.
     *
     * @param File $file
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function __construct(File $file)
    {
        $this->fileDriver = $file;
        $this->now = date('Y-m-d');
        $this->expireDate = date('Y-m-d', strtotime(" +" . self::TOKEN_EXPIRE_TIME));

        if ($token = $this->loadToken()) {
            self::$token['key'] = json_decode($token)->key;
            self::$token['expire_date'] = json_decode($token)->expire_date;
        }

        // Create token if there is no one stored in file.
        if (!self::$token) {
            $this->createToken();
        }
    }

    /**
     * Retrieve an updated token.
     *
     * @return array
     */
    public function getUpdatedToken()
    {
        if ($this->now >= self::$token['expire_date']) {
            $this->createToken();
        }

        return self::$token;
    }

    /**
     * Compare 2 tokens(strings).
     *
     * @param $token
     * @return bool
     */
    public function isTokenValid($token)
    {
        return $token === $this->getUpdatedToken()['key'];
    }

    /**
     * Create token.
     *
     * @return int
     */
    private function createToken()
    {
        $token = substr(base64_encode(md5( mt_rand() )), 0, 24);

        self::$token['key'] = 'Bearer ' . $token;
        self::$token['expire_date'] = $this->expireDate;

        // Store token in file.
        return $this->storeToken(self::$token);
    }

    /**
     * Store token in file.
     *
     * @param $token
     * @return int
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    private function storeToken($token)
    {
        try {
            $result = $this->fileDriver->filePutContents(self::TOKEN_FILE_NAME, json_encode($token));
        } catch (\FileSystemException $e) {
            return false;
        }

        return $result;
    }

    /**
     * Load token from file.
     *
     * @return string
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    private function loadToken()
    {
        try {
            $result = $this->fileDriver->fileGetContents(self::TOKEN_FILE_NAME);
        } catch (\FileSystemException $e) {
            return false;
        }

        return $result;
    }
}